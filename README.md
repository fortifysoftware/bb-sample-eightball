# BitBucket sample pipeline - EightBall

*Please run the `./run-after-clone.sh` script after cloning this repository*

This repository provides a sample BitBucket Pipeline for scanning the EightBall example and reporting scan results.
Please see the contents of the [bitbucket-pipelines.yml](bitbucket-pipelines.yml) file for more information.

This repository is derived from https://github.com/fortify/sample-eightball.git using using https://github.com/rsenden/repo-as-template. 
To update this repository with updated content from https://github.com/fortify/sample-eightball.git, please run `./repo-as-template/pull.sh`.
